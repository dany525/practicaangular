import { NodeCompatibleEventEmitter } from 'rxjs/internal/observable/fromEvent';

export class Todo{
    
    id:number;
    title:string;
    completed:boolean;

}