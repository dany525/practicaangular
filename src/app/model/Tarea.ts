import { NodeCompatibleEventEmitter } from 'rxjs/internal/observable/fromEvent';

export class Tarea{
    
    tarea_id:number;
    descripcion:string;
    fecha_ejecucion:string;
    fecha_creacion:string;
    estado:boolean;
    usuario_id:number;

}