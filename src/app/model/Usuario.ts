import { NodeCompatibleEventEmitter } from 'rxjs/internal/observable/fromEvent';

export class Usuario{
    
    id_usuario:number;
    nombres:string;
    apellidos:string;
    fecha_creacion:string;
    email:string;
    estado:boolean;

}