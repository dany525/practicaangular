import { NgModule } from '@angular/core';
import{Routes,RouterModule} from '@angular/router';
import { CommonModule } from '@angular/common';
import{UsuariosComponent} from'./components/usuarios/usuarios.component';
import{UsuarioListarBindearComponent} from './components/usuario-listar-bindear/usuario-listar-bindear.component';
import{TareasComponent} from'./components/tareas/tareas/tareas.component';
import{TareasEnviarEventoComponent} from'./components/tareas/tareas-enviar-evento/tareas-enviar-evento.component';


const routes: Routes =[

  { path:'agregarUsuario',component:UsuariosComponent},
  { path:'listarUsuarios',component:UsuarioListarBindearComponent},
  { path:'',component:TareasComponent},
  { path:'agregarTarea',component:TareasEnviarEventoComponent}

];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports:[RouterModule]
})
export class AppRoutingModule { 



}
