import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { Usuario } from 'src/app/model/Usuario';
import { NgForm,FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-agregar-usuario',
  templateUrl: './agregar-usuario.component.html',
  styleUrls: ['./agregar-usuario.component.css']
})
export class AgregarUsuarioComponent implements OnInit {


  @Output() agregarUsuario: EventEmitter<any> =  new EventEmitter;

  constructor() { 

  }

  ngOnInit() {


  }
   onClickSubmit(data) {

    this.agregarUsuario.emit(data);

      console.log("Entered Email id : " + data.nombres);
   }



}
