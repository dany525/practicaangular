import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsuarioListarBindearComponent } from './usuario-listar-bindear.component';

describe('UsuarioListarBindearComponent', () => {
  let component: UsuarioListarBindearComponent;
  let fixture: ComponentFixture<UsuarioListarBindearComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsuarioListarBindearComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsuarioListarBindearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
