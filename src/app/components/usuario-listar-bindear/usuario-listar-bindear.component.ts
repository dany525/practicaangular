import { Component, OnInit } from '@angular/core';

import { SubjectSubscriber } from 'rxjs/internal/Subject';
import { UsuariosService } from 'src/app/services/Usuarios.service';
import {Usuario} from '../../model/Usuario';
import { Subscriber } from 'rxjs';

@Component({
  selector: 'app-usuario-listar-bindear',
  templateUrl: './usuario-listar-bindear.component.html',
  styleUrls: ['./usuario-listar-bindear.component.css']
})
export class UsuarioListarBindearComponent implements OnInit {
  usuarios:Usuario[];
  constructor(private usuariosService:UsuariosService) { }

  ngOnInit() {

    this.usuariosService.getTodos().subscribe(usuarios =>{
      this.usuarios = usuarios;
      console.log(this.usuarios)

    });

  }


  deleteUsuario(usuario:Usuario){

    //remove from UI

    this.usuarios = this.usuarios.filter(t=> t.id_usuario !== usuario.id_usuario);

    //remove from SERVER
    
    this.usuariosService.deleteUsuario(usuario).subscribe();
   

  }
  
}
