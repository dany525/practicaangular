import { Component, OnInit,Input,EventEmitter,Output } from '@angular/core';
import { UsuariosService } from 'src/app/services/Usuarios.service';
import {Usuario} from 'src/app/model/Usuario';


@Component({
  selector: 'app-listar-usuarios',
  templateUrl: './listar-usuarios.component.html',
  styleUrls: ['./listar-usuarios.component.css']
})
export class ListarUsuariosComponent implements OnInit {
  @Input() usuario:Usuario;
  @Output() deleteUsuario: EventEmitter<Usuario> = new EventEmitter();
  constructor(private usuariosService:UsuariosService) { 
  }

  ngOnInit() {

    console.log(this.usuario);
  }

  onDelete(usuario){


    this.deleteUsuario.emit(usuario);

    console.log(usuario)

  }



}
