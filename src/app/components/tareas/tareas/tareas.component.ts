import { Component, OnInit } from '@angular/core';
import { SubjectSubscriber } from 'rxjs/internal/Subject';
import { TareasService } from 'src/app/services/tareas.service';
import {Tarea} from 'src/app/model/Tarea';
import { Subscriber } from 'rxjs';

@Component({
  selector: 'app-tareas',
  templateUrl: './tareas.component.html',
  styleUrls: ['./tareas.component.css']
})
export class TareasComponent implements OnInit {
  tareas:Tarea[];
  constructor(private tareasService:TareasService) { }

  ngOnInit() {

    this.tareasService.getTodosTareas().subscribe(tareas =>{
      this.tareas = tareas;
    });

  }

  deleteTarea(tarea:Tarea){

    //remove from UI
    this.tareas = this.tareas.filter(t=> t.tarea_id !== tarea.tarea_id);
    //remove from server
    this.tareasService.deleteTarea(tarea).subscribe();

  }


  

}
