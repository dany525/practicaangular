import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TareasListarComponent } from './tareas-listar.component';

describe('TareasListarComponent', () => {
  let component: TareasListarComponent;
  let fixture: ComponentFixture<TareasListarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TareasListarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TareasListarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
