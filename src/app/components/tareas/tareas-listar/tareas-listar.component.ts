import { Component, OnInit,Input,EventEmitter,Output } from '@angular/core';
import{Tarea} from 'src/app/model/Tarea';
import { TareasService } from 'src/app/services/Tareas.service';


@Component({
  selector: 'app-tareas-listar',
  templateUrl: './tareas-listar.component.html',
  styleUrls: ['./tareas-listar.component.css']
})
export class TareasListarComponent implements OnInit {
  @Input() tarea:Tarea[];

  @Output() deleteTarea: EventEmitter<Tarea> = new EventEmitter();

  constructor(private tareasService:TareasService) { 
  }
  
  ngOnInit() {
    console.log(this.tarea);
  }

  onDelete(tarea:Tarea){

  console.log(tarea)

    this.deleteTarea.emit(tarea);

    console.log(tarea)

  }



}
