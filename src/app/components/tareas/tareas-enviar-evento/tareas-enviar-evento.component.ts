import { Component, OnInit } from '@angular/core';
import { SubjectSubscriber } from 'rxjs/internal/Subject';
import { TareasService } from 'src/app/services/tareas.service';
import {Tarea} from 'src/app/model/Tarea';
import { Subscriber } from 'rxjs';

@Component({
  selector: 'app-tareas-enviar-evento',
  templateUrl: './tareas-enviar-evento.component.html',
  styleUrls: ['./tareas-enviar-evento.component.css']
})
export class TareasEnviarEventoComponent implements OnInit {

  tareas:Tarea[];
  constructor(private tareasService:TareasService) { }

  ngOnInit() {

    this.tareasService.getTodosTareas().subscribe(tareas =>{
      this.tareas = tareas;
    });

  }



  agregarTarea(tarea:Tarea){

    this.tareasService.agregarTarea(tarea).subscribe(usuario =>{
      this.tareas.push(tarea)

  });

  }



}
