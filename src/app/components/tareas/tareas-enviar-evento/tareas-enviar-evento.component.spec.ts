import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TareasEnviarEventoComponent } from './tareas-enviar-evento.component';

describe('TareasEnviarEventoComponent', () => {
  let component: TareasEnviarEventoComponent;
  let fixture: ComponentFixture<TareasEnviarEventoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TareasEnviarEventoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TareasEnviarEventoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
