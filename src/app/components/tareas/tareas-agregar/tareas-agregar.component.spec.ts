import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TareasAgregarComponent } from './tareas-agregar.component';

describe('TareasAgregarComponent', () => {
  let component: TareasAgregarComponent;
  let fixture: ComponentFixture<TareasAgregarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TareasAgregarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TareasAgregarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
