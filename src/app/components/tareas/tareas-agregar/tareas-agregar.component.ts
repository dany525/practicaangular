import { Component, OnInit ,Output,EventEmitter} from '@angular/core';

@Component({
  selector: 'app-tareas-agregar',
  templateUrl: './tareas-agregar.component.html',
  styleUrls: ['./tareas-agregar.component.css']
})
export class TareasAgregarComponent implements OnInit {
  @Output() agregarTarea: EventEmitter<any> =  new EventEmitter;



  constructor() { }

  ngOnInit() {
  }


  onClickSubmit(data) {

    this.agregarTarea.emit(data);

     
   }

 






}
