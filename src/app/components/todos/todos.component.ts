import { Component, OnInit } from '@angular/core';
import {Todo} from '../../model/Todo';
import { TodoService } from '../../services/Todo.service'
import { Subscriber } from 'rxjs';
import { log } from 'util';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {
  todos:Todo[];

  constructor(private todoService:TodoService) {}


  ngOnInit() {

    this.todoService.getTodos().subscribe(todos =>{
    this.todos = todos;
    });

  }

  deleteTodo(todo:Todo){

    //remove from UI

    this.todos = this.todos.filter(t=> t.id !== todo.id);

    //remove from SERVER
    
    this.todoService.deleteTodo(todo).subscribe();

    console.log('delete me');


  }

  addTodo(todo:Todo){

    this.todoService.addTodo(todo).subscribe(todo =>{
      this.todos.push(todo)

    })


  }



}
