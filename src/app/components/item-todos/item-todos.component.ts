import { Component, OnInit,Input,EventEmitter,Output } from '@angular/core';
import { TodoService } from '../../services/Todo.service'
import { Todo } from 'src/app/model/Todo';

@Component({
  selector: 'app-item-todos',
  templateUrl: './item-todos.component.html',
  styleUrls: ['./item-todos.component.css']
})
export class ItemTodosComponent implements OnInit {

  @Input() todo:Todo;
  @Output() deleteTodo: EventEmitter<Todo> = new EventEmitter();

  constructor(private todoService:TodoService) { }

  ngOnInit() {
  }

  //condicional cuando venga la visita cumplida
  
  setClasses(){

    let clase ={

        todo: true,
        'is-complete': this.todo.completed

    }

    return clase;

  }

  //en evento cambio el completado
  onToggle(todo){

    todo.completed = !todo.completed;
    this.todoService.ToggleComplet(todo).subscribe(todo => console.log(todo));

  }

  onDelete(todo){


    this.deleteTodo.emit(todo);

    console.log(todo)

  }

}
