import { Component, OnInit } from '@angular/core';
import { SubjectSubscriber } from 'rxjs/internal/Subject';
import { UsuariosService } from 'src/app/services/Usuarios.service';
import {Usuario} from '../../model/Usuario';
import { Subscriber } from 'rxjs';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {
  usuarios:Usuario[];

  constructor(private usuariosService:UsuariosService) { }
  ngOnInit() {

    this.usuariosService.getTodos().subscribe(usuarios =>{
      this.usuarios = usuarios;

    });

  }

  deleteUsuario(usuario:Usuario){

    //remove from UI

    this.usuarios = this.usuarios.filter(t=> t.id_usuario !== usuario.id_usuario);

    //remove from SERVER
    
    this.usuariosService.deleteUsuario(usuario).subscribe();
   

  }


  agregarUsuario(usuario:Usuario){


      this.usuariosService.agregarUsuario(usuario).subscribe(usuario =>{
      this.usuarios.push(usuario)

    })

    }



}
