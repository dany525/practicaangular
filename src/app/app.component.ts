import { Component } from '@angular/core';
import { UsuariosService } from 'src/app/services/Usuarios.service';
import {Usuario} from 'src/app/model/Usuario';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  usuarios:[Usuario];

  constructor(private usuariosService:UsuariosService){
    
  }

  changeName(title:string){


  }

  agregarUsuario(usuario:Usuario){
    
    this.usuariosService.agregarUsuario(usuario).subscribe(usuario =>{
    this.usuarios.push(usuario)

  })
  

  }
  
}
