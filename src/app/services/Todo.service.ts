import { Injectable } from '@angular/core';
import{Observable} from 'rxjs';
import{ HttpClient, HttpHeaders} from '@angular/common/http';
import{Todo } from '../model/Todo';

const httpOptions ={

  headers: new HttpHeaders({
  'Content-Type':'application/json'
  })

}


@Injectable({
  providedIn: 'root'
})

export class TodoService {

  //url listado
  todosUrl:string = 'https://jsonplaceholder.typicode.com/todos?_limit=5';

  //url put
  todosUrl2:string = 'https://jsonplaceholder.typicode.com/todos';


  constructor(private http:HttpClient) { }
  //all
  getTodos():Observable<Todo[]>{

    return this.http.get<Todo[]>(this.todosUrl);
  }

  //Delete todo

  deleteTodo(todo:Todo):Observable<Todo> {
    //remove from UI
    const url = `${this.todosUrl2}/${todo.id}`;
    //remove from server
    return this.http.delete<Todo>(url,httpOptions);

  }


  addTodo(todo:Todo):Observable<Todo>{

    return this.http.post<Todo>(this.todosUrl2,todo,httpOptions);

  }


  //ToggleCompleted

  ToggleComplet(todo: Todo):Observable<any>{
    
    const url = `${this.todosUrl2}/${todo.id}`;

    return this.http.put(url,todo,httpOptions);
  }



}
