import { Injectable } from '@angular/core';
import{Observable} from 'rxjs';
import{ HttpClient, HttpHeaders} from '@angular/common/http';
import{Tarea } from 'src/app/model/Tarea';


const httpOptions ={

  headers: new HttpHeaders({
  'Content-Type':'application/json'
  })

}


@Injectable({
  providedIn: 'root'
})
export class TareasService {

   //url listado
  // tareaUrl:string = 'http://192.168.1.78:80/apiphp/index.php?m=usuario&f=getTareas';
    server:string = 'http://localhost';
    tareaUrl:string =this.server+':8080/Tarea';
    //url Eliminar
    //tareaUrl2:string = 'http://192.168.1.78:80/apiphp/index.php?m=usuario&f=deleteTarea&id';
    tareaUrl2:string = this.server+':8080/Tarea/borrarTarea?idTarea';
      //url Eliminar
      //tareaUrl3:string = 'http://192.168.1.78:80/apiphp/index.php?m=usuario&f=agregarTarea&descripcion';
    tareaUrl3:string = this.server+':8080/Tarea/crearTarea?descripcion';


  constructor(private http:HttpClient) { }

  getTodosTareas():Observable<Tarea[]>{

    return this.http.get<Tarea[]>(this.tareaUrl);

    }


     //agregarUsuario
     agregarTarea(tarea:Tarea):Observable<Tarea> {
      console.log('entre');
      console.log(tarea);
 
      let url = `${this.tareaUrl3}=${tarea.descripcion}`;
      let url2 = `${url}&fecha_ejecucion=${tarea.fecha_ejecucion}`;
      let url3 = `${url2}&user=${tarea.usuario_id}`;
    
     return this.http.post<Tarea>(url3,httpOptions);
  
    }




  //Eliminar de user interfaz
    deleteTarea(tarea:Tarea):Observable<Tarea> {
   
      const url = `${this.tareaUrl2}=${tarea.tarea_id}`;
     
     return this.http.delete<Tarea>(url,httpOptions);
  
    }


 

}
