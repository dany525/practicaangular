import { Injectable } from '@angular/core';

import{Observable} from 'rxjs';
import{ HttpClient, HttpHeaders} from '@angular/common/http';
import{Usuario } from '../model/Usuario';

const httpOptions ={

  headers: new HttpHeaders({
  'Content-Type':'application/json'
  })

}

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {
    server:string = 'http://localhost';
    //url listado
   // usuarioUrl:string = 'http://192.168.1.78:80/apiphp/index.php?m=usuario&f=getUser';
      usuarioUrl:string =this.server+':8080/Usuarios';


     //url Eliminar
     //usuarioUrl2:string = 'http://192.168.1.78:80/apiphp/index.php?m=usuario&f=deleteUser&id';
     usuarioUrl2:string =this.server+':8080/Usuarios/borrarUsuario?idUsuario';

     //url Agregar
     //usuarioUrl3:string = 'http://192.168.1.78:80/apiphp/index.php?m=usuario&f=agregarUsuario&nombre';
     usuarioUrl3:string =this.server+':8080/Usuarios/crearUsuario?nombres';

    constructor(private http:HttpClient) { }
    //all
    getTodos():Observable<Usuario[]>{

    return this.http.get<Usuario[]>(this.usuarioUrl);

    }
    //Eliminar de user interfaz
    deleteUsuario(usuario:Usuario):Observable<Usuario> {
   
      const url = `${this.usuarioUrl2}=${usuario.id_usuario}`;
     
     return this.http.delete<Usuario>(url,httpOptions);
  
    }

       //agregarUsuario
       agregarUsuario(usuario:Usuario):Observable<Usuario> {
        console.log('entre');
        console.log(usuario);
   
        let url = `${this.usuarioUrl3}=${usuario.nombres}`;
        let url2 = `${url}&email=${usuario.email}`;
        let url3 = `${url2}&apellidos=${usuario.apellidos}`;
       return this.http.put<Usuario>(url3,httpOptions);
    
      }





}





