import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import{HttpClientModule} from '@angular/common/http';
import{FormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { TodosComponent } from './components/todos/todos.component';
import { ItemTodosComponent } from './components/item-todos/item-todos.component';
import { HeaderComponent } from './components/Layout/header/header.component';
import { AddTodoComponent } from './components/add-todo/add-todo.component';
import { UsuariosComponent } from './components/usuarios/usuarios.component';
import { ListarUsuariosComponent } from './components/listar-usuarios/listar-usuarios.component';
import { AgregarUsuarioComponent } from './components/agregar-usuario/agregar-usuario.component';
import { AppRoutingModule } from './app-routing.module';
import { UsuarioListarBindearComponent } from './components/usuario-listar-bindear/usuario-listar-bindear.component';
import { TareasComponent } from './components/tareas/tareas/tareas.component';
import { TareasListarComponent } from './components/tareas/tareas-listar/tareas-listar.component';
import { TareasAgregarComponent } from './components/tareas/tareas-agregar/tareas-agregar.component';
import { TareasEnviarEventoComponent } from './components/tareas/tareas-enviar-evento/tareas-enviar-evento.component';





@NgModule({
  declarations: [
    AppComponent,
    TodosComponent,
    ItemTodosComponent,
    HeaderComponent,
    AddTodoComponent,
    UsuariosComponent,
    ListarUsuariosComponent,
  
    AgregarUsuarioComponent,
    UsuarioListarBindearComponent,
    TareasComponent,
    TareasListarComponent,
    TareasAgregarComponent,
    TareasEnviarEventoComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
